/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenda;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author ivoag
 */
public class ContatoAmigo extends Contato {
    List <Reuniao> reunioes ;
    
  
    public ContatoAmigo(){
        this.reunioes = new ArrayList(); 
        super.dtnasc = new Data();
    }

    public List<Reuniao> getReunioes() {
        return this.reunioes;
    }

    public void setReunioes(List<Reuniao> reunioes) {
        this.reunioes = reunioes;
    }
    
  
}
