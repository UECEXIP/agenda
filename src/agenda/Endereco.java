
package agenda;


public class Endereco {
    String rua,numero,complemento,bairro,cep,cidade,estado,pais;
   
    public Endereco(String rua, String numero, String complemento, String bairro, String cep, String cidade, String estado, String pais){
        this.rua = rua;
        this.numero = numero;
        this.complemento = complemento;
        this.bairro = bairro;
        this.cep = cep;
        this.cidade = cidade;
        this.estado = estado;
        this. pais = pais;
    }
    
     Endereco(){
    
}
    public String getRua(){
        return rua;
    }
    
    public void setRua(String street){
        this.rua = street;
    }
    
    public String getNumero(){
        return numero;
    }
    
    public void setNumero( String num){
        this.numero = num;
    }
    
    public String getComplemento(){
        return complemento;
    }
    
    public void setComplemento( String compl){
        this.complemento = compl;
    }
    
    public String getBairro(){
        return bairro;
    }
    
    public void setBairro(String block){
        this.bairro = block;
    }
    
    public String getCEP(){
       return cep;
    }
    
    public void setCEP(String cid){
        this.cep = cid;
    }
    
    public String getCidade(){
        return cidade;
    }
    
    public void setCidade(String city){
        this.cidade = city;
    }
    
    public String getEstado(){
        return estado;
    }
    
    public void setEstado(String state){
        this.estado = state;
    }
    
    public String getPais(){
        return pais;
    }
    
    public void setPais(String country){
        this.pais =country;
    }
    
    public void mostrarEndereco(){
        System.out.println("Rua : "+rua);
        System.out.println("Numero da residencia: "+ numero);
        System.out.println("Complemento: " + complemento);
        System.out.println("Bairro: " + bairro);
        System.out.println("CEP: "+ cep);
        System.out.println("Cidade: "+ cidade);
        System.out.println("Estado: "+ estado);
        System.out.println("Pais: "+ pais);
    }
}
