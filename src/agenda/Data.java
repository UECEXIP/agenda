/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenda;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Data {
    public int dia;
    public int mes;
    public int ano;
    
     Data(int dia, int mes, int ano){
        this.dia = dia;
        this.mes= mes;
        this.ano = ano;
    }
    
     Data(){
        
    }
    
    public int getDia(){
       return dia;
    }
    
    public void setDia(int d){
        this.dia = d;
    }
    
    public int getMes(){
        return mes;
    }
    
    public void setMes( int m){
        this.mes = m;
    }
    
    public int getAno(){
        return ano;
    }
    
    public void setAno( int year){
        this.ano = year;
    }
    
    
    public int contaDias(Data data){
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        long ml1 = 0;
        try{
            
            Date data1 = sdf.parse(dia+"/"+mes+"/"+ano);
            Date data2 = sdf.parse(data.dia+"/"+data.mes+"/"+data.ano);
            Calendar c = new GregorianCalendar();
            ml1= data1.getTime() - data2.getTime();
            ml1 = ((((ml1/1000)/60)/60)/24);            
        
        }catch(ParseException e){e.printStackTrace();}
        
        return (int) ml1;
    }
    
    public String ToString(){
        return dia+"/"+mes+"/"+ ano;
    }
    
   
           
    
    
    
}
