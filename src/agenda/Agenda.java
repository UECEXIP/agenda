package agenda;

/**
 *
 * @author ivoag
 */
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Agenda {

    Scanner entrada;
    static List<Contato> listadecontatos;

    public static void main(String[] args) {
        listadecontatos = new ArrayList<>();
        Scanner entrada = new Scanner(System.in);
        int menu;
        //MENU--------------------------------------------------------------------
        do {
            System.out.println("|====== Menu Principal ======|\n");
            System.out.println("|1|- Adicionar contatos      |");
            System.out.println("|2|- Exibir Contatos         |");
            System.out.println("|3|- Editar Contatos         |");
            System.out.println("|4|- Excluir Contatos        |");
            System.out.println("|5|- Buscar Contatos         |");
            System.out.println("|6|- Sair                    |");
            System.out.print("Opção: ");
            menu = entrada.nextInt();
            if (menu == 1) {
                addContato();
            }
            if (menu == 2) {
                System.out.println("Lista de Contatos");
                for (int i = 0; i < listadecontatos.size(); i++) {
                    System.out.println(i + " - " + listadecontatos.get(i).getNome());
                }
                System.out.println("Digite o índice do contato que voce deseja ver:");
                int contatoEscolhido = entrada.nextInt();
                mostrarContato(listadecontatos.get(contatoEscolhido));
            }
            if (menu == 3) {
                editarContato();
            }
            if (menu == 4) {
                RemoveContato();
            }
            if (menu == 5) {
                System.out.println("Pesquisar Contato: ");
                System.out.println("Insira o nome, o numero ou email do contato que voce está procurando.");
                String contatoEscolhido = entrada.nextLine();
                contatoEscolhido = entrada.nextLine();
                buscaContatoNome(contatoEscolhido);
            }

        } while (menu != 6);
    }
    //MENU----------------------------------------------------------------------FIM
    // MOSTRAR CONTATO---------------------------------------------------------------------------------------INICIO
    public static void mostrarContato(Contato contato) {
        System.out.println("-----------------------------EXIBIR CONTATO-------------------------------------");
        System.out.println("Grupo: " + contato.grupo);
        System.out.println("Nome: " + contato.nome);
        for (int i = 0; i < listadecontatos.size(); i++) {
            System.out.println("Email: " + contato.emails);
        }

        for (int i = 0; i < contato.telefones.size(); i++) {
            System.out.println("Operadora: " + contato.telefones.get(i).operadora);
            System.out.println("Telefone: (" + contato.telefones.get(i).dd + ") " + contato.telefones.get(i).numero);
        }
        System.out.println("Rua: " + contato.endereco.rua);
        System.out.println("Numero: " + contato.endereco.numero);
        System.out.println("Complemento: " + contato.endereco.complemento);
        System.out.println("Bairro: " + contato.endereco.bairro);
        System.out.println("CEP: " + contato.endereco.cep);
        System.out.println("Cidade: " + contato.endereco.cidade);
        System.out.println("Estado: " + contato.endereco.estado);
        System.out.println("País:: " + contato.endereco.pais);
        System.out.println("Nascimento: " + contato.dtnasc.dia + "/" + contato.dtnasc.mes + "/" + contato.dtnasc.ano);
        if (contato instanceof ContatoAmigo) {
            ContatoAmigo ctta = (ContatoAmigo) contato;
            for (int i = 0; i < ctta.reunioes.size(); i++) {
                System.out.println("Data da Reuniao: " + ctta.reunioes.get(i).data.dia + "/" + ctta.reunioes.get(i).data.mes + "/" + ctta.reunioes.get(i).data.ano);
                System.out.println("Assunto: " + ctta.reunioes.get(i).titulo);
                System.out.println("Mensagem: " + ctta.reunioes.get(i).mensagem);
            }

        }
        if (contato instanceof ContatoFamilia) {
            ContatoFamilia cttf = (ContatoFamilia) contato;
            System.out.println("Parentesco: " + cttf.parentesco);
        }

        if (contato instanceof ContatoTrabalho) {
            ContatoTrabalho cttt = (ContatoTrabalho) contato;
            System.out.println("Empresa: " + cttt.setor);
            System.out.println("Setor: " + cttt.empresa);
            for (int i = 0; i < cttt.reunioes.size(); i++) {
                System.out.print("Data da Reuniao: " + cttt.reunioes.get(i).data.dia + "/" + cttt.reunioes.get(i).data.mes + "/" + cttt.reunioes.get(i).data.ano);
                System.out.println("Assunto: " + cttt.reunioes.get(i).titulo);
                System.out.println("Mensagem: " + cttt.reunioes.get(i).mensagem);
            }

        }

    }
    // MOSTRAR CONTATO---------------------------------------------------------------------------------------FIM
    // ADD CONTATO---------------------------------------------------------------------------------------INICIO
    public static void addContato() {

        Scanner teclado = new Scanner(System.in);
        System.out.println("-----------------------------ADICIONAR CONTATO-------------------------------------");
        System.out.println("Insira o grupo o qual o seu Contato pertence por meio do numero respectivo::");
        System.out.println("|1| - Familia\n|2| - Amigo\n|3| - Trabalho");
        System.out.print("Opção: ");
        int grupo;
        int saidaWhile = 0;
        do {
            grupo = teclado.nextInt();
            if (grupo == 1 || grupo == 2 || grupo == 3) {
                saidaWhile = 1;
            }
        } while (saidaWhile != 1);
        if (grupo == 1) {
            ContatoFamilia cttf = new ContatoFamilia();
            cttf.grupo = "Familia";
            System.out.print("Insira o nome do Contato: ");
            cttf.nome = teclado.nextLine();
            cttf.nome = teclado.nextLine();
            int opt=1;
            while(opt!=2){
                System.out.println("Deseja adicionar outro Telefone: ");
                 System.out.println("1 - SIM     2-NAO");
                 opt = teclado.nextInt();
                if(opt==1){
                Telefone t1 = new Telefone();
                System.out.print("Insira a operadora: ");
                t1.setOperadora(teclado.nextLine());
                t1.setOperadora(teclado.nextLine());
                System.out.print("Insira  Código de Área: ");
                t1.setDD(teclado.nextLine());
                System.out.print("Insira  o número do Telefone: ");
                t1.setNumero(teclado.nextLine());
                cttf.addTelefone(t1);
                } else if(opt != 2){
                    System.out.println("Opcao inválida:");
                    opt= teclado.nextInt();
                }
        }
            System.out.println("Vamos inserir o endereço.");
            System.out.print("Rua: ");
            String rua = teclado.nextLine();
             rua = teclado.nextLine();
            System.out.print("Numero: ");
            String numero = teclado.nextLine();
            System.out.print("Complemento: ");
            String complemento = teclado.nextLine();
            System.out.print("Bairro: ");
            String bairro = teclado.nextLine();
            System.out.print("Cep: ");
            String cep = teclado.nextLine();
            System.out.print("Cidade: ");
            String cidade = teclado.nextLine();
            System.out.print("Estado: ");
            String estado = teclado.nextLine();
            System.out.print("Pais: ");
            String pais = teclado.nextLine();
            Endereco e1 = new Endereco(rua, numero, complemento, bairro, cep, cidade, estado, pais);
            cttf.endereco = e1;
            int op = 1;
            while(op != 2){
                System.out.println("Deseja adicionar um email? 1 sim 2 nao");
                op = teclado.nextInt();
                if(op == 1){
                    System.out.print("Digite o email: ");
                    cttf.addEmail(teclado.nextLine());
                    cttf.addEmail(teclado.nextLine());
                }else if(op !=2){
                    System.out.print("Numero invalido. Digite novamente. ");
                }
            }
            System.out.println("Vamos inserir a Data do aniversário do Contato.");
            System.out.print("Dia: ");
            int dia = teclado.nextInt();
            System.out.print("Mes: ");
            int mes = teclado.nextInt();
            System.out.print("Ano: ");
            int ano = teclado.nextInt();
            Data d1 = new Data(dia, mes, ano);
            cttf.dtnasc = d1;
            System.out.print("Insira o seu grau de parentesco com o Contato: ");
            cttf.parentesco = teclado.nextLine();
            cttf.parentesco = teclado.nextLine();
            listadecontatos.add(cttf);
            System.out.print("Contato Salvo com Sucesso!!!");
        }

        if (grupo == 2) {
            ContatoAmigo ctta = new ContatoAmigo();
            ctta.grupo = "Amigo";
            System.out.print("Insira o nome do Contato: ");
            ctta.nome = teclado.nextLine();
            ctta.nome = teclado.nextLine();
             int opt=1;
            while(opt!=2){
                System.out.println("Deseja adicionar outro Telefone: ");
                 System.out.println("1 - SIM     2-NAO");
                 opt = teclado.nextInt();
                if(opt==1){
                Telefone t1 = new Telefone();
                System.out.print("Insira a operadora: ");
                t1.setOperadora(teclado.nextLine());
                t1.setOperadora(teclado.nextLine());
                System.out.print("Insira  Código de Área: ");
                t1.setDD(teclado.nextLine());
                System.out.print("Insira  o número do Telefone: ");
                t1.setNumero(teclado.nextLine());
                ctta.addTelefone(t1);
                } else if(opt != 2){
                    System.out.println("Opcao inválida:");
                    opt= teclado.nextInt();
                }
        }
            System.out.println("Vamos inserir o endereço:");
            System.out.print("Rua: ");
            String rua = teclado.nextLine();
            rua = teclado.nextLine();
            System.out.print("Numero: ");
            String numero = teclado.nextLine();
            System.out.print("Complemento: ");
            String complemento = teclado.nextLine();
            System.out.print ("Bairro: ");
            String bairro = teclado.nextLine();
            System.out.print ("Cep: ");
            String cep = teclado.nextLine();
            System.out.print("Cidade: ");
            String cidade = teclado.nextLine();
            System.out.print ("Estado: ");
            String estado = teclado.nextLine();
            System.out.print("Pais: ");
            String pais = teclado.nextLine();
            Endereco e1 = new Endereco(rua, numero, complemento, bairro, cep, cidade, estado, pais);
            ctta.endereco = e1;
            int op = 1;
            while(op != 2){
                System.out.println("Deseja adicionar um email? 1 sim 2 nao");
                op = teclado.nextInt();
                if(op == 1){
                    System.out.print("Digite o email: ");
                    ctta.addEmail(teclado.nextLine());
                    ctta.addEmail(teclado.nextLine());
                }else if(op !=2){
                    System.out.print("Numero invalido. Digite novamente. ");
                }
            }
            System.out.println("Vamos inserir a Data do aniversário do Contato:");
            System.out.print("Dia: ");
            int dia = teclado.nextInt();
            System.out.print("Mes: ");
            int mes = teclado.nextInt();
            System.out.print("Ano: ");
            int ano = teclado.nextInt();
            Data d1 = new Data(dia, mes, ano);
            ctta.dtnasc = d1;
            int opr;
             System.out.println("|1|. Desjo inserir reuniao    |2|.Nao desejo inserir reuniao");
             opr = teclado.nextInt();
            if(opr ==1){
               System.out.println("Vamos inserir a Reuniao:");
                Reuniao r1 = new Reuniao();
                System.out.println("Data:");
                System.out.print("Dia: ");
                r1.data.dia = teclado.nextInt();
                System.out.print("Mes: ");
                r1.data.mes = teclado.nextInt();
                System.out.print("Ano: ");
                r1.data.ano = teclado.nextInt();
                System.out.println("Assunto: ");
                r1.titulo = teclado.nextLine();
                r1.titulo = teclado.nextLine();
            }else if (opr != 2) {
                 System.out.println("Opcão inválida");
               
            }
            listadecontatos.add(ctta);
            System.out.println("Reunião salva!");
        }

        if (grupo == 3) {
            ContatoTrabalho cttt = new ContatoTrabalho();
            cttt.grupo = "Trabalho";
            System.out.print("Insira o nome do Contato: ");
            cttt.nome = teclado.nextLine();
            cttt.nome = teclado.nextLine();
            int opt=1;
            while(opt!=2){
                System.out.println("Deseja adicionar outro Telefone: ");
                 System.out.println("1 - SIM     2-NAO");
                 opt = teclado.nextInt();
                if(opt==1){
                Telefone t1 = new Telefone();
                System.out.print("Insira a operadora: ");
                t1.setOperadora(teclado.nextLine());
                t1.setOperadora(teclado.nextLine());
                System.out.print("Insira  Código de Área: ");
                t1.setDD(teclado.nextLine());
                System.out.print("Insira  o número do Telefone: ");
                t1.setNumero(teclado.nextLine());
                cttt.addTelefone(t1);
                } else if(opt != 2){
                    System.out.println("Opcao inválida:");
                    opt= teclado.nextInt();
                }
        }
            System.out.println("Vamos inserir o endereço:");
            System.out.print("Rua: ");
            String rua = teclado.nextLine();
            rua = teclado.nextLine();
            System.out.print("Numero: ");
            String numero = teclado.nextLine();
            System.out.print("Complemento:");
            String complemento = teclado.nextLine();
            System.out.print("Bairro: ");
            String bairro = teclado.nextLine();
            System.out.print("Cep: ");
            String cep = teclado.nextLine();
            System.out.print("Cidade: ");
            String cidade = teclado.nextLine();
            System.out.print("Estado: ");
            String estado = teclado.nextLine();
            System.out.print("Pais: ");
            String pais = teclado.nextLine();
            Endereco e1 = new Endereco(rua, numero, complemento, bairro, cep, cidade, estado, pais);
            cttt.endereco = e1;
            int op = 1;
            while(op != 2){
                System.out.println("Deseja adicionar um email? 1 sim 2 nao");
                op = teclado.nextInt();
                if(op == 1){
                    System.out.print("Digite o email: ");
                    cttt.addEmail(teclado.nextLine());
                    cttt.addEmail(teclado.nextLine());
                }else if(op !=2){
                    System.out.print("Numero invalido. Digite novamente. ");
                }
            }
            System.out.println("Vamos inserir a Data do aniversário do Contato:");
            System.out.print("Dia: ");
            int dia = teclado.nextInt();
            System.out.print("Mes: ");
            int mes = teclado.nextInt();
            System.out.print("Ano: ");
            int ano = teclado.nextInt();
            Data d1 = new Data(dia, mes, ano);
            cttt.dtnasc = d1;
            System.out.print("Insira o setor: ");
            cttt.setor = teclado.nextLine();
            cttt.setor = teclado.nextLine();
            System.out.print("Insira Empresa: ");
            cttt.empresa = teclado.nextLine();
            int opr;
             System.out.println("|1|. Desjo inserir reuniao    |2|.Nao desejo inserir reuniao");
             opr = teclado.nextInt();
            if(opr ==1){
               System.out.println("Vamos inserir a Reuniao:");
                Reuniao r1 = new Reuniao();
                System.out.println("Data da Reuniao:");
                System.out.print("Dia: ");
                r1.data.dia = teclado.nextInt();
                System.out.print("Mes: ");
                r1.data.mes = teclado.nextInt();
                System.out.print("Ano: ");
                r1.data.ano = teclado.nextInt();
                System.out.println("Assunto: ");
                r1.titulo = teclado.nextLine();
                r1.titulo = teclado.nextLine();
            }else if (opr != 2) {
                 System.out.println("Opcão inválida");
               
            }

            listadecontatos.add(cttt);
        }
    }
  // ADD CONTATO-------------------------------------------------------------------------------------------------FIM  
  // BUSCA CONTATO-----------------------------------------------------------------------------------------------INICIO
    public static void buscaContatoNome(String nome) {
        System.out.println("-----------------------------PESQUISAR CONTATO-------------------------------------");
        ContatoAmigo ctta = new ContatoAmigo();
        ContatoFamilia cttf = new ContatoFamilia();
        ContatoTrabalho cttt = new ContatoTrabalho();
        for (int i = 0; i < listadecontatos.size(); i++) {
            Contato ctt = listadecontatos.get(i);
            int teste = 0;
            if (ctt.getNome().contains(nome)) {
                teste = 1;
            }
            for (int tel = 0; tel < ctt.telefones.size(); tel++) {
                if (ctt.telefones.get(tel).numero.contains(nome)) {
                    teste = 1;
                }
            }
            for (int email = 0; email < ctt.telefones.size(); email++) {
                if (ctt.emails.get(email).contains(nome)) {
                    teste = 1;
                }
            }
            if (teste == 1) {
                mostrarContato(listadecontatos.get(i));
            }
        }
    }
    // BUSCA CONTATO -----------------------------------------------------------------------------------------------------------------------------FIM
    //REMOVE CONTATO-----------------------------------------------------------------------------------------------------------------------------INCIO
    public static void RemoveContato() {
        System.out.println("-----------------------------REMOVER CONTATO-------------------------------------");
        Scanner teclado = new Scanner(System.in);
        System.out.println("Lista de Contatos: ");
        for (int i = 0; i < listadecontatos.size(); i++) {
            System.out.println(i + " - " + listadecontatos.get(i).getNome());
        }
        System.out.println("Insira o indice do contato que se deseja apagar:");
        int indice = teclado.nextInt();
        listadecontatos.remove(indice);
    }
    //REMOVE CONTATO------------------------------------------------------------------------------------------------------------------------------FIM
    public static void EditarContato() {
        Scanner teclado = new Scanner(System.in);
        ContatoAmigo ctta = new ContatoAmigo();
        ContatoTrabalho cttt = new ContatoTrabalho();
        ContatoFamilia cttf = new ContatoFamilia();
        System.out.println("Lista de Contatos:");
        for (int i = 0; i < listadecontatos.size(); i++) {
            System.out.println(i + " - " + listadecontatos.get(i).getNome());
        }

        System.out.println("Insira qual o indice que voce deseja editar: ");
        int contatoSelecionado = teclado.nextInt();
        System.out.println("Voce escolheu");
        mostrarContato(listadecontatos.get(contatoSelecionado));
        if (listadecontatos.get(contatoSelecionado) instanceof ContatoFamilia) {
            cttf = (ContatoFamilia) listadecontatos.get(contatoSelecionado);
            int num;
            System.out.println("O que voce deseja editar: ");
            System.out.println("1.Nome  " + "2.Telefone  " + "3.Email  " + "4.Endereco  " + "5.Data de aniversário  " + "6.Parentsco  ");
            System.out.print("\n");
            System.out.println("Insira o numero referente ao que voce deseja editar ");
            num = teclado.nextInt();
            while (num != 1 || num != 2 || num != 3 || num != 4 || num != 5 || num != 6) {
                System.out.println("Opção inválida");
                num = teclado.nextInt();
            }
            if (num == 1) {
                System.out.println("Insira o novo nome:");
                cttf.setNome(teclado.nextLine());
                cttf.setNome(teclado.nextLine());
            }
            if (num == 2) {
                for (int i = 0; i < listadecontatos.size(); i++) {
                    System.out.println(i + " - " + listadecontatos.get(i).telefones);
                }
                System.out.println("Digite o índice do Telefone que voce deseja editar: ");
                int telefoneSelecionado = teclado.nextInt();
                System.out.println("Insira o numero respectivo ao que voce deseja editar: ");
                System.out.println("1.Operadora  " + "2.Codigo de area:  " + "3.Numero  ");
                System.out.print("\n");
                int opcaoDoTelefone = teclado.nextInt();
                if (opcaoDoTelefone != 1 || opcaoDoTelefone != 2 || opcaoDoTelefone != 3) {
                    System.out.println("1.Operadora  " + "2.Codigo de area:  " + "3.Numero  ");
                    System.out.println("Insira o numero respectivo ao que voce deseja editar: ");
                    opcaoDoTelefone = teclado.nextInt();
                }
                if (opcaoDoTelefone == 1) {
                    System.out.println("Insira a nova operadora: ");
                    cttf.telefones.get(telefoneSelecionado).setOperadora(teclado.nextLine());
                }
                if (opcaoDoTelefone == 2) {
                    System.out.println("Insira o novo Código de Área: ");
                    cttf.telefones.get(telefoneSelecionado).setDD(teclado.nextLine());
                }
                if (opcaoDoTelefone == 3) {
                    System.out.println("Insira o novo número: ");
                    cttf.telefones.get(telefoneSelecionado).setNumero(teclado.nextLine());
                }
            }

            if (num == 3) {
                System.out.println("Insira o((s) novo(s) email: ");
                for (int i = 0; i < listadecontatos.size(); i++) {
                    System.out.println(listadecontatos.get(i).emails);
                }
            }

            if (num == 4) {
                System.out.println("1.Rua" + "2.Numero " + "3.Complemeto " + "4.Bairro  " + "5.CEP  " + "6.Cidade  " + "7.Estado  " + "8.País  ");
                System.out.println("Digite do índice do Endereco que voce deseja editar:");
                System.out.printf("\n");
                int opcaoDoEndereco = teclado.nextInt();
                if (opcaoDoEndereco != 1 || opcaoDoEndereco != 2 || opcaoDoEndereco != 3 || opcaoDoEndereco != 4 || opcaoDoEndereco != 5 || opcaoDoEndereco != 6 || opcaoDoEndereco != 7 || opcaoDoEndereco != 8) {
                    System.out.println("1.Rua" + "2.Numero " + "3.Complemeto " + "4.Bairro  " + "5.CEP  " + "6.Cidade  " + "7.Estado  " + "8.País  ");
                }
                System.out.println("Digite do índice do Endereco que voce deseja editar:");
                opcaoDoEndereco = teclado.nextInt();
                if (opcaoDoEndereco == 1) {
                    System.out.println("Insira a nova Rua do Contato: ");
                    cttf.endereco.setRua(teclado.nextLine());
                }

                if (opcaoDoEndereco == 2) {
                    System.out.println("Insira o novo Numero de residencia do Contato: ");
                    cttf.endereco.setNumero(teclado.nextLine());
                }
                if (opcaoDoEndereco == 3) {
                    System.out.println("Insira o novo complemento da Residencia do Contato");
                    cttf.endereco.setComplemento(teclado.nextLine());
                }

                if (opcaoDoEndereco == 4) {
                    System.out.println("Insira o novo Bairro do Contato: ");
                    cttf.endereco.setBairro(teclado.nextLine());
                }

                if (opcaoDoEndereco == 5) {
                    System.out.println("Insira o novo CEP do contato: ");
                    cttf.endereco.setCEP(teclado.nextLine());
                }

                if (opcaoDoEndereco == 6) {
                    System.out.println("Insira o novo Município do contato: ");
                    cttf.endereco.setCidade(teclado.nextLine());
                }

                if (opcaoDoEndereco == 7) {
                    System.out.println("Insira o novo Estado do Contato: ");
                    cttf.endereco.setEstado(teclado.nextLine());
                }

                if (opcaoDoEndereco == 8) {
                    System.out.println("Insira o novo País do Contato: ");
                    cttf.endereco.setPais(teclado.nextLine());
                }
            }
            if (num == 5) {
                System.out.println("1.Dia  " + "2.Mes  " + "3.Ano");
                System.out.println("Insira o valor referente ao dado que se deseja modificar: ");
                int opcaoDeData = teclado.nextInt();
                if (opcaoDeData != 1 || opcaoDeData != 2 || opcaoDeData != 3) {
                    System.out.println("1.Dia  " + "2.Mes  " + "3.Ano");
                    System.out.println("Insira o valor referente ao dado que se deseja modificar: ");
                    opcaoDeData = teclado.nextInt();
                }
                if (opcaoDeData == 1) {
                    System.out.println("Insira o novo dia da data de aniversário: ");
                    cttf.dtnasc.setDia(teclado.nextInt());
                }

                if (opcaoDeData == 2) {
                    System.out.println("Insira o novo mes da Data de Aniversário: ");
                    cttf.dtnasc.setMes(teclado.nextInt());
                }

                if (opcaoDeData == 3) {
                    System.out.println("Insira o novo ano referente a Data de aniversário");
                    cttf.dtnasc.setAno(opcaoDeData);
                }
            }

            if (num == 6) {
                System.out.println("Insira o grau de parentesco que voce deseja moficar: ");
                cttf.setParentesco(teclado.nextLine());
            }

        }

        if (listadecontatos.get(contatoSelecionado) instanceof ContatoAmigo) {
            ctta = (ContatoAmigo) listadecontatos.get(contatoSelecionado);
            int num;
            System.out.println("1.Nome  " + "2.Telefone  " + "3.Email  " + "4.Endereco  " + "5.Data de aniversário  ");
            System.out.println("Insira o valor do Índice que deseja editar: : ");
            num = teclado.nextInt();
            if (num != 1 || num != 2 || num != 3 || num != 4 || num != 5) {
                System.out.println("1.Nome  " + "2.Telefone  " + "3.Email  " + "4.Endereco  " + "5.Data de aniversário  ");
                System.out.println("Insira o valor do Índice que deseja editar: : ");
                num = teclado.nextInt();
            }
            if (num == 1) {
                System.out.println("Insira o novo nome do contato:");
                ctta.setNome(teclado.nextLine());
            }

            if (num == 2) {
                for (int i = 0; i < listadecontatos.size(); i++) {
                    System.out.println(i + " - " + listadecontatos.get(i).telefones);
                }
                System.out.println("Digite o índice do Telefone que voce deseja editar: ");
                int telefoneSelecionado = teclado.nextInt();
                System.out.println("1.Operadora  " + "2.Codigo de area:  " + "3.Numero  ");
                System.out.println("Insira o numero respectivo ao que voce deseja editar: ");
                System.out.print("\n");
                int opcaoDoTelefone = teclado.nextInt();
                if (opcaoDoTelefone != 1 || opcaoDoTelefone != 2 || opcaoDoTelefone != 3) {
                    System.out.println("1.Operadora  " + "2.Codigo de area:  " + "3.Numero  ");
                    System.out.println("Insira o numero respectivo ao que voce deseja editar: ");
                    opcaoDoTelefone = teclado.nextInt();
                }
                if (opcaoDoTelefone == 1) {
                    System.out.println("Insira a nova operadora do contato: ");
                    ctta.telefones.get(telefoneSelecionado).setOperadora(teclado.nextLine());
                }

                if (opcaoDoTelefone == 2) {
                    System.out.println("Insira o novo Código de Área do Contato: ");
                    ctta.telefones.get(telefoneSelecionado).setDD(teclado.nextLine());
                }

                if (opcaoDoTelefone == 3) {
                    System.out.println("Insira o novo Numero do contato: ");
                    ctta.telefones.get(telefoneSelecionado).setNumero(teclado.nextLine());
                }

            }

            if (num == 3) {
                System.out.println("Insira o novo email do Contato:");
                for (int i = 0; i < listadecontatos.size(); i++) {
                    System.out.println(listadecontatos.get(i).emails);
                }
            }

            if (num == 4) {
                System.out.println("1.Rua" + "2.Numero " + "3.Complemeto " + "4.Bairro  " + "5.CEP  " + "6.Cidade  " + "7.Estado  " + "8.País  ");
                System.out.println("Digite do índice do que voce deseja editar:");
                System.out.printf("\n");
                int opcaoDoEndereco = teclado.nextInt();
                if (opcaoDoEndereco != 1 || opcaoDoEndereco != 2 || opcaoDoEndereco != 3 || opcaoDoEndereco != 4 || opcaoDoEndereco != 5 || opcaoDoEndereco != 6 || opcaoDoEndereco != 7 || opcaoDoEndereco != 8) {
                    System.out.println("1.Rua  " + "2.Numero " + "3.Complemeto " + "4.Bairro  " + "5.CEP  " + "6.Cidade  " + "7.Estado  " + "8.País  ");
                    System.out.println("Digite do índice do que voce deseja editar:");
                    opcaoDoEndereco = teclado.nextInt();
                }
                if (opcaoDoEndereco == 1) {
                    System.out.println("Insira a nova Rua do Contato: ");
                    ctta.endereco.setRua(teclado.nextLine());
                }

                if (opcaoDoEndereco == 2) {
                    System.out.println("Insira o novo Numero de residencia do Contato: ");
                    ctta.endereco.setNumero(teclado.nextLine());
                }
                if (opcaoDoEndereco == 3) {
                    System.out.println("Insira o novo complemento da Residencia do Contato");
                    ctta.endereco.setComplemento(teclado.nextLine());
                }
                if (opcaoDoEndereco == 4) {
                    System.out.println("Insira o novo Bairro do Contato: ");
                    ctta.endereco.setBairro(teclado.nextLine());
                }

                if (opcaoDoEndereco == 5) {
                    System.out.println("Insira o novo CEP do contato: ");
                    ctta.endereco.setCEP(teclado.nextLine());
                }

                if (opcaoDoEndereco == 6) {
                    System.out.println("Insira o novo Município do contato: ");
                    ctta.endereco.setCidade(teclado.nextLine());
                }

                if (opcaoDoEndereco == 7) {
                    System.out.println("Insira o novo Estado do Contato: ");
                    ctta.endereco.setEstado(teclado.nextLine());
                }

                if (opcaoDoEndereco == 8) {
                    System.out.println("Insira o novo País do Contato: ");
                    ctta.endereco.setPais(teclado.nextLine());
                }
            }

            if (num == 5) {
                System.out.println("1.Dia  " + "2.Mes  " + "3.Ano");
                System.out.println("Insira o valor referente ao dado que se deseja modificar: ");
                int opcaoDeData = teclado.nextInt();
                if (opcaoDeData != 1 || opcaoDeData != 2 || opcaoDeData != 3) {
                    System.out.println("1.Dia  " + "2.Mes  " + "3.Ano");
                    System.out.println("Insira o valor referente ao dado que se deseja modificar: ");
                    opcaoDeData = teclado.nextInt();
                }
                if (opcaoDeData == 1) {
                    System.out.println("Insira o novo dia da data de aniversário: ");
                    ctta.dtnasc.setDia(teclado.nextInt());
                }

                if (opcaoDeData == 2) {
                    System.out.println("Insira o novo mes da Data de Aniversário: ");
                    ctta.dtnasc.setMes(teclado.nextInt());
                }

                if (opcaoDeData == 3) {
                    System.out.println("Insira o novo ano referente a Data de aniversário");
                    ctta.dtnasc.setAno(opcaoDeData);
                }

            }
        }

        if (listadecontatos.get(contatoSelecionado) instanceof ContatoAmigo) {
            cttt = (ContatoTrabalho) listadecontatos.get(contatoSelecionado);
            int num;
            System.out.println("1.Nome  " + "2.Telefone  " + "3.Email  " + "4.Endereco  " + "5.Data de aniversário  " + "6.Setor:  " + "7.Empresa  ");
            System.out.println("Insira o índice referente ao que se deseja editar:");
            num = teclado.nextInt();
            if (num != 1 || num != 2 || num != 3 || num != 4 || num != 5 || num != 6 || num != 7) {
                System.out.println("1.Nome  " + "2.Telefone  " + "3.Email  " + "4.Endereco  " + "5.Data de aniversário  " + "6.Setor:  " + "7.Empresa  ");
                System.out.println("Insira o índice referente ao que se deseja editar:");
                num = teclado.nextInt();
            }
            if (num == 1) {
                System.out.println("Insira o novo nome do contato:");
                cttt.setNome(teclado.nextLine());
            }
            if (num == 2) {
                for (int i = 0; i < listadecontatos.size(); i++) {
                    System.out.println(i + " - " + listadecontatos.get(i).telefones);
                }
                System.out.println("Digite o índice do Telefone que voce deseja editar: ");
                int telefoneSelecionado = teclado.nextInt();
                System.out.println("1.Operadora  " + "2.Codigo de area:  " + "3.Numero  ");
                System.out.println("Insira o numero do índice que voce deseja editar: ");
                System.out.print("\n");
                int opcaoDoTelefone = teclado.nextInt();
                if (opcaoDoTelefone != 1 || opcaoDoTelefone != 2 || opcaoDoTelefone != 3) {
                    System.out.println("1.Operadora  " + "2.Codigo de area:  " + "3.Numero  ");
                    System.out.println("Insira o numero do índice que voce deseja editar: ");
                    opcaoDoTelefone = teclado.nextInt();
                }
                if (opcaoDoTelefone == 1) {
                    System.out.println("Insira a nova operadora do contato: ");
                    cttt.telefones.get(telefoneSelecionado).setOperadora(teclado.nextLine());
                }

                if (opcaoDoTelefone == 2) {
                    System.out.println("Insira o novo Código de Área do Contato: ");
                    cttt.telefones.get(telefoneSelecionado).setDD(teclado.nextLine());
                }

                if (opcaoDoTelefone == 3) {
                    System.out.println("Insira o novo Numero do contato: ");
                    cttt.telefones.get(telefoneSelecionado).setNumero(teclado.nextLine());
                }

            }
            if (num == 3) {
                System.out.println("Insira o novo email do Contato:");
                for (int i = 0; i < listadecontatos.size(); i++) {
                    System.out.println(listadecontatos.get(i).emails);
                }
            }
            if (num == 4) {
                System.out.println("1.Rua" + "2.Numero " + "3.Complemeto " + "4.Bairro  " + "5.CEP  " + "6.Cidade  " + "7.Estado  " + "8.País  ");
                System.out.println("Digite do índice que voce deseja editar:");
                System.out.printf("\n");
                int opcaoDoEndereco = teclado.nextInt();
                if (opcaoDoEndereco != 1 || opcaoDoEndereco != 2 || opcaoDoEndereco != 3 || opcaoDoEndereco != 4 || opcaoDoEndereco != 5 || opcaoDoEndereco != 6 || opcaoDoEndereco != 7 || opcaoDoEndereco != 8) {
                    System.out.println("1.Rua" + "2.Numero " + "3.Complemeto " + "4.Bairro  " + "5.CEP  " + "6.Cidade  " + "7.Estado  " + "8.País  ");
                    System.out.println("Digite do índice que voce deseja editar:");
                    opcaoDoEndereco = teclado.nextInt();
                }
                if (opcaoDoEndereco == 1) {
                    System.out.println("Insira a nova Rua do Contato: ");
                    cttt.endereco.setRua(teclado.nextLine());
                }

                if (opcaoDoEndereco == 2) {
                    System.out.println("Insira o novo Numero de residencia do Contato: ");
                    cttt.endereco.setNumero(teclado.nextLine());
                }
                if (opcaoDoEndereco == 3) {
                    System.out.println("Insira o novo complemento da Residencia do Contato");
                    cttt.endereco.setComplemento(teclado.nextLine());
                }
                if (opcaoDoEndereco == 4) {
                    System.out.println("Insira o novo Bairro do Contato: ");
                    cttt.endereco.setBairro(teclado.nextLine());
                }

                if (opcaoDoEndereco == 5) {
                    System.out.println("Insira o novo CEP do contato: ");
                    cttt.endereco.setCEP(teclado.nextLine());
                }

                if (opcaoDoEndereco == 6) {
                    System.out.println("Insira o novo Município do contato: ");
                    cttt.endereco.setCidade(teclado.nextLine());
                }

                if (opcaoDoEndereco == 7) {
                    System.out.println("Insira o novo Estado do Contato: ");
                    cttt.endereco.setEstado(teclado.nextLine());
                }

                if (opcaoDoEndereco == 8) {
                    System.out.println("Insira o novo País do Contato: ");
                    cttt.endereco.setPais(teclado.nextLine());
                }
            }

            if (num == 5) {
                System.out.println("1.Dia  " + "2.Mes  " + "3.Ano");
                System.out.println("Insira o valor referente ao Índice que se deseja modificar: ");
                int opcaoDeData = teclado.nextInt();
                if (opcaoDeData != 1 || opcaoDeData != 2 || opcaoDeData != 3) {
                    System.out.println("1.Dia  " + "2.Mes  " + "3.Ano");
                    System.out.println("Insira o valor referente ao Índice que se deseja modificar: ");
                    opcaoDeData = teclado.nextInt();
                }
                if (opcaoDeData == 1) {
                    System.out.println("Insira o novo dia da data de aniversário: ");
                    cttt.dtnasc.setDia(teclado.nextInt());
                }

                if (opcaoDeData == 2) {
                    System.out.println("Insira o novo mes da Data de Aniversário: ");
                    cttt.dtnasc.setMes(teclado.nextInt());
                }

                if (opcaoDeData == 3) {
                    System.out.println("Insira o novo ano referente a Data de aniversário");
                    cttt.dtnasc.setAno(opcaoDeData);
                }

            }

            if (num == 6) {
                System.out.println("Insira o novo setor do Contato: ");
                cttt.setSetor(teclado.nextLine());
            }

            if (num == 7) {
                System.out.println("Insira a nova Empresa em que o Contato trabalha ");
                cttt.setEmpresa(teclado.nextLine());
            }
        }
    }

    public static void editarContato() {
        Scanner ler = new Scanner(System.in);
        for (int i = 0; i < listadecontatos.size(); i++) {
            System.out.println(i + " - " + listadecontatos.get(i).getNome());
        }
        System.out.print("Digite o indice do contato que deseja editar: ");
        int pessoaSelecionada = ler.nextInt();
        Contato contato = listadecontatos.get(pessoaSelecionada);
        mostrarContato(contato);
        System.out.println("\n");
        System.out.println("Escolha o que voce quer editar.");
        System.out.print("1 - Nome, 2 - Grupo, 3 - Endereco, 4 - Nascimento, 5 - Email, 6 - Telefone ");
        if (contato instanceof ContatoAmigo) {System.out.print(",7 - Reuniao");}     
        if (contato instanceof ContatoTrabalho) {System.out.print(",7 - Reuniao, 8 - Empresa, 9 - Setor");}
        if (contato instanceof ContatoFamilia) {System.out.print(",7 - Parentesco");}
        int opcao = ler.nextInt();
       
        if(opcao == 1){
            System.out.println("Digite o novo Nome do contato");
            contato.setNome(ler.nextLine());
        }
        if(opcao == 2){
            System.out.println("Digite o indice referente a mudança de Grupo que o contato ira receber.");
            System.out.println(" |1|-  Familia   |2|-  Amigo    |3|-  Trabalho");
            int op = 1;
            String grupo="";
            while (op != 1  || op!=2  || op!=3){
                 op = ler.nextInt();
            if(op ==1){
                grupo = "Familia";
                RemoveContato();
                addContato();
            }else
            if(op ==2){
                grupo = "Amigo";
                  RemoveContato();
                addContato();
            }else
            if(op ==3){
                grupo = "Trabalho";
                  RemoveContato();
                addContato();
            }else{
                System.out.print("Opcao invalida");
            }
            }  
            contato.setGrupo(grupo);
        }
        
        if (opcao == 3) {
            System.out.println("Digite o novo Endereco do contato");
            Endereco e1 = new Endereco();
            System.out.print("Digite rua ");
            e1.setRua(ler.nextLine());
            System.out.print("Digite Numero da residencia: ");
            e1.setNumero(ler.nextLine());
            System.out.println("Digite o Complemento: ");
            e1.setComplemento(ler.nextLine());
            System.out.println("Digite o Bairro ");
            e1.setBairro(ler.nextLine());
            System.out.println("Digite a CEP: ");
             e1.setCEP(ler.nextLine());
            System.out.println("Digite a cidade: ");
            e1.setCidade(ler.nextLine());
            System.out.println("Digite a estado: ");
            e1.setEstado(ler.nextLine());
            System.out.println("Digite a pais: ");
            e1.setPais(ler.nextLine());
        }
        if (opcao == 4) {
            Data d1 = new Data();
            System.out.println("Digite a nova Data de Nascimento do contato");
            System.out.print("Digite o novo dia do nascimento: ");
            d1.setDia(opcao);
            System.out.print("Digite o novo mes do nascimento: ");
            d1.setMes(opcao);
            System.out.print("Digite o novo ano do nascimento: ");
            d1.setAno(opcao);
           
        }
        if (opcao == 5) {
            for(int i=0; i< contato.getEmails().size(); i++){
                System.out.println(i+" " +contato.getEmails().get(i));
            }
            System.out.println("Qual email voce deseja editar:");
            int indice = ler.nextInt();
            contato.getEmails().remove(indice);
            System.out.println("Insira o novo email:");
            String newmail = ler.nextLine();
            contato.getEmails().add(newmail);
            contato.setEmails(contato.getEmails());
        }

        if (opcao == 6) {
            for(int i=0; i< contato.getTelefone().size(); i++){
                System.out.println(i+" " +contato.getTelefone().get(i).numero);
            }
            Telefone t1 = new Telefone();
            System.out.println("Qual telefone voce deseja editar:");
            int indice = ler.nextInt();
            contato.getTelefone().remove(indice);
            System.out.print ("Digite o nova Operadora");
            t1.setOperadora(ler.nextLine());
            System.out.print ("Digite o nova Codigo de Area");
            t1.setDD(ler.nextLine());
            System.out.print ("Digite o novo numero");
            t1.setNumero(ler.nextLine());
            contato.setTelefone(contato.getTelefone());
        }
                                                
        if (contato instanceof ContatoAmigo) {
            ContatoAmigo ctta = (ContatoAmigo) contato;
            if(opcao == 7){
                for(int i =0; i<ctta.reunioes.size(); i++){
                    System.out.println(i+"-"+ctta.reunioes.get(i).data.ToString()+")"+ctta.reunioes.get(i).titulo) ;
                }
                System.out.println("Qual a reuniao que voce deseja editar:");
                opcao = ler.nextInt();
                ctta.getReunioes().remove(opcao);
                Reuniao r1  = new Reuniao();
                System.out.println("Insira a nova data da reuniao:");
                Data data = new Data();
                System.out.println("Insira o dia da reunia:");
                data.setDia(ler.nextInt());
                System.out.println("Insira o mes da reunia:");
                data.setMes(ler.nextInt());
                System.out.println("Insira o ano da reunia:");
                  data.setAno(ler.nextInt());
                  System.out.println("Insira o novo titulo:");
                  r1.setTitulo(ler.nextLine());
                  ctta.getReunioes().add(r1);
                  ctta.setReunioes(ctta.getReunioes());
            }
            listadecontatos.remove(pessoaSelecionada);
            listadecontatos.add(ctta);
        }     
        if (contato instanceof ContatoTrabalho) {
            ContatoTrabalho cttt = (ContatoTrabalho) contato;
            if(opcao == 7){
                for(int i =0; i<cttt.reunioes.size(); i++){
                    System.out.println(i+"-"+cttt.reunioes.get(i).data.ToString()+")"+cttt.reunioes.get(i).titulo) ;
                }
                System.out.println("Qual a reuniao que voce deseja editar:");
                opcao = ler.nextInt();
                cttt.getReunioes().remove(opcao);
                Reuniao r1  = new Reuniao();
                System.out.println("Insira a nova data da reuniao:");
                Data data = new Data();
                System.out.println("Insira o dia da reunia:");
                data.setDia(ler.nextInt());
                System.out.println("Insira o mes da reunia:");
                data.setMes(ler.nextInt());
                System.out.println("Insira o ano da reunia:");
                  data.setAno(ler.nextInt());
                  System.out.println("Insira o novo titulo:");
                  r1.setTitulo(ler.nextLine());
                  cttt.getReunioes().add(r1);
                  cttt.setReunioes(cttt.getReunioes());
            }
            
            if(opcao == 8){
             System.out.println("Digite a nova empresa em que o Contato");
             cttt.setSetor(ler.nextLine());
             
            }
            
            if(opcao == 9){
                System.out.println("Digite o novo setor do Contato");
                cttt.setEmpresa(ler.nextLine()); 
            }
             listadecontatos.remove(pessoaSelecionada);
             listadecontatos.add(cttt);
        }   
        if (contato instanceof ContatoFamilia) {
            ContatoFamilia cttf = (ContatoFamilia) contato;
            if(opcao ==7){
                System.out.println("Insira o novo grau deparentesco do Contato:");
                cttf.setParentesco(ler.nextLine());
                 }
            listadecontatos.remove(pessoaSelecionada);
            listadecontatos.add(cttf);
        }
    
    }
    
  
 } 




